﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using UIKit;
using Foundation;

namespace SearchExample
{
	public class SearchAppDataSource : UITableViewDataSource
	{
		List<SearchAppModel> _Data;

		List<SearchAppModel> _filtered;


		public SearchAppDataSource()
		{
			ReturnClear = false;
			_Data = Inits();
		}

		List<SearchAppModel> Inits()
		{
			var res = new List<SearchAppModel>(20000);
			var d = Gen();
			var dd = Gen();
			for (int i = 0; i < 20000; i++)
			{
				res.Add(new SearchAppModel() { Title = d[i], DescriptionTxt = dd[i]});
			}
			return res;
		}

		public List<string> Gen()
		{
			var res = new List<string>(20000);
			for (var i = 0; i < 20000; i++) 
			{
				res.Add(Guid.NewGuid().ToString("N"));
			}
			return res;
		}

		#region Properties
		public List<SearchAppModel> Data 
		{ 
			get 
			{ 
				return (_filtered == null || !_filtered.Any()) ? _Data.Take(200).ToList() : _filtered.ToList();
			} 
			set 
			{
				_filtered = value;
			}
		}

		public string FilterText { get; set; }

		public bool ReturnClear { get; private set; }

		#endregion

		public int Filter(string d)
		{
			if (string.IsNullOrWhiteSpace(d))
			{
				if (_filtered == null) { _filtered = new List<SearchAppModel>(); }
				_filtered.Clear();
				FilterText = d;
				ReturnClear = false;
				return -1;
			}

			FilterText = d;
			_filtered = _Data.Where(w => w.Title.Contains(FilterText) || w.DescriptionTxt.Contains(FilterText)).ToList();
			if (!_filtered.Any())
			{
				ReturnClear = true;
			}
			else { 
				ReturnClear = false;
			}
			return _filtered.Count();

		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var firstAttributes = new UIStringAttributes
			{
				ForegroundColor = UIColor.FromRGB(255, 255, 255),
				BackgroundColor = UIColor.FromRGB(32, 99, 150),
				Font = UIFont.FromName("Courier", 18f)
			};
			var _cell = tableView.DequeueReusableCell(Constants.CellId_AutolayoutTrick);
			if (_cell == null)
			{
				_cell = new UITableViewCell(UITableViewCellStyle.Subtitle, Constants.CellId_AutolayoutTrick);
			}
			var item = Data[indexPath.Row];

			if (!string.IsNullOrWhiteSpace(FilterText))
			{
				var titlePresent = item.Title.IndexOf(FilterText, StringComparison.CurrentCultureIgnoreCase);
				if (titlePresent != -1)
				{
					var attrStr = new NSMutableAttributedString(item.Title);
					attrStr.AddAttributes(firstAttributes, new NSRange(titlePresent, FilterText.Length));
					_cell.TextLabel.AttributedText = attrStr;
				}
				else {
					_cell.TextLabel.AttributedText = new NSMutableAttributedString(item.Title);
				}

				var descPresent = item.DescriptionTxt.IndexOf(FilterText, StringComparison.CurrentCultureIgnoreCase);
				if (descPresent != -1)
				{
					var attrStr = new NSMutableAttributedString(item.DescriptionTxt);
					attrStr.AddAttributes(firstAttributes, new NSRange(descPresent, FilterText.Length));
					_cell.DetailTextLabel.AttributedText = attrStr;
				}
				else {
					_cell.DetailTextLabel.AttributedText = new NSMutableAttributedString(item.DescriptionTxt);
				}
			}
			else 
			{ 
				_cell.TextLabel.AttributedText = new NSMutableAttributedString(item.Title);
				_cell.DetailTextLabel.AttributedText = new NSMutableAttributedString(item.DescriptionTxt);
			}

			_cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
			_cell.BackgroundColor = UIColor.FromRGB(255, 175, 143);
			_cell.SetNeedsUpdateConstraints();
			_cell.UpdateConstraints();
			return _cell;
		}


		public override nint RowsInSection(UITableView tableView, nint section)
		{
			return (ReturnClear) ? 0 : Data.Count();
		}
	}
}


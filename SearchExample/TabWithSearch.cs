﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Foundation;

namespace SearchExample
{
	public partial class TabWithSearch : UIViewController
	{
		
		public TabWithSearch() : base(Constants.VcId_Main, null)
		{
		}

		#region Properties
		public SearchAppDataSource DataSource { get; set;}
		#endregion

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			DataSource = new SearchAppDataSource();
			TableForItems.DataSource = DataSource;
			AppSearchBar.Delegate = new AppSearchBarBehaviour(Test, CancelFilter, ErrorMethod);
			//--
			SetActivityWidghtAndSpacing(0,0);
		}

		void SetActivityWidghtAndSpacing(int widght, int spacing)
		{
			UIView.AnimateAsync(0.4, () =>
			{
				SpinnerWidght.Constant = widght;
				Spinner.SetNeedsUpdateConstraints();
				Spinner.UpdateConstraints();
				DistanceBetweenSpinnerAndSearch.Constant = spacing;
				View.NeedsUpdateConstraints();
				View.LayoutSubviews();
			});
		}

		void ErrorMethod(bool show, string message = "We need more chars")
		{
			UIView.Transition(ErrLabel, 0.5, UIViewAnimationOptions.TransitionCrossDissolve, () =>
			{
					if (show)
					{
						var firstAttributes = new UIStringAttributes
						{
							ForegroundColor = UIColor.FromRGB(255, 255, 255),
							BackgroundColor = UIColor.FromRGB(233, 93, 37),
							Font = UIFont.FromName("Courier", 16f)
						};
						var text = new NSMutableAttributedString(message);
						text.AddAttributes(firstAttributes, new NSRange(0, message.Length));
						ErrLabel.AttributedText = text;
					}
					else 
				{ ErrLabel.AttributedText = new NSMutableAttributedString(string.Empty); }
			},() => {});
		}

		void CancelFilter(string txtToSearch)
		{
			DataSource.Filter(null);
			TableForItems.ReloadData();
		}

		public async Task<bool> Test(string some)
		{
			SetActivityWidghtAndSpacing(20, 20);
			Spinner.StartAnimating();
			CancelFilter(null);
			var dsRes = DataSource.Filter(some);
			var res = await Task.Run(() =>
			{
				Thread.Sleep(5000);
				return false;
			});
			Notify(dsRes, false);
				TableForItems.ReloadData();

			Spinner.StopAnimating();
			SetActivityWidghtAndSpacing(0, 0);
			return res;
		}

		void Notify(int dsRes, bool isFinal)
		{
			if (dsRes > 0)
			{
				if (dsRes > 200)
				{
					ErrorMethod(true, "Please clarify your search query");
					return;
				}
				ErrorMethod(false);
			}
			if (dsRes == 0)
			{
				ErrorMethod(true, "Not found");
			}
			else
			{
				ErrorMethod(false);
			}
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}



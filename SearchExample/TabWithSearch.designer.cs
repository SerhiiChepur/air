// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SearchExample
{
	[Register ("TabWithSearch")]
	partial class TabWithSearch
	{
		[Outlet]
		UIKit.UISearchBar AppSearchBar { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint DistanceBetweenSpinnerAndSearch { get; set; }

		[Outlet]
		UIKit.UILabel ErrLabel { get; set; }

		[Outlet]
		UIKit.UIView RootView { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView Spinner { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint SpinnerWidght { get; set; }

		[Outlet]
		UIKit.UITableView TableForItems { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DistanceBetweenSpinnerAndSearch != null) {
				DistanceBetweenSpinnerAndSearch.Dispose ();
				DistanceBetweenSpinnerAndSearch = null;
			}

			if (AppSearchBar != null) {
				AppSearchBar.Dispose ();
				AppSearchBar = null;
			}

			if (RootView != null) {
				RootView.Dispose ();
				RootView = null;
			}

			if (SpinnerWidght != null) {
				SpinnerWidght.Dispose ();
				SpinnerWidght = null;
			}

			if (TableForItems != null) {
				TableForItems.Dispose ();
				TableForItems = null;
			}

			if (ErrLabel != null) {
				ErrLabel.Dispose ();
				ErrLabel = null;
			}

			if (Spinner != null) {
				Spinner.Dispose ();
				Spinner = null;
			}
		}
	}
}

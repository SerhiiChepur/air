﻿using UIKit;
using System.Threading.Tasks;
using Foundation;

namespace SearchExample
{
	public delegate Task TxtFilter(string txtToSearch);
	public delegate void TxtFilterReset(string txtToSearch);
	public delegate void Err(bool show, string msg = "We need more chars");

	public class AppSearchBarBehaviour : UISearchBarDelegate
	{
		private ObjCRuntime.Selector selector;
		public AppSearchBarBehaviour(TxtFilter txtFilterMethod, TxtFilterReset resetFilter, Err errorM)
		{
			FilterMethod = txtFilterMethod;
			ResetMethod = resetFilter;
			ErrorMethod = errorM;
		}

		public TxtFilter FilterMethod { get; set; }
		public Err ErrorMethod { get;  set; }
		public TxtFilterReset ResetMethod { get; set; }

		[Export("doFilter:")]
		public async void PrepareForInterfaceBuilder(string s)
		{
			if (FilterMethod != null)
			{
				await FilterMethod(s);
			}
		}

		public override void TextChanged(UISearchBar searchBar, string searchText)
		{
			if (searchText.Length < 3) 
			{
				CancelPreviousPerformRequest(this);
				if (ResetMethod != null)
				{
					ResetMethod(null);
				}
				if (ErrorMethod != null)
				{
					if(searchText.Length == 0) ErrorMethod(false);
					if (searchText.Length > 0) ErrorMethod(true);
				}
				return; 
			}
			ErrorMethod(false);
			selector = new ObjCRuntime.Selector("doFilter:");
			CancelPreviousPerformRequest(this);

			PerformSelector(selector, new NSString(searchText), 0.7);
		}

		public override void CancelButtonClicked(UISearchBar searchBar)
		{
			CancelPreviousPerformRequest(this);
			if (ErrorMethod != null)
			{
				ErrorMethod(false);
			}
			if (ResetMethod != null)
			{
				ResetMethod(null);
			}
		}


	}
}
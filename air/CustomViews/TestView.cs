using Foundation;
using System;
using UIKit;
using ObjCRuntime;

namespace air
{
    public partial class TestView : UIView
    {
		private nfloat _previousHeight;
        public TestView (IntPtr handle) : base (handle)
        {
			
        }
		public UITableView TTable
		{
			get 
			{
				return TestTable;
			}
		}

		public static TestView Create()
		{
			var arr = NSBundle.MainBundle.LoadNib("TestView", null, null);
			var v = Runtime.GetNSObject<TestView>(arr.ValueAt(0));
			return v;
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();
		}

		public nfloat RecalcHeight()
		{
			_previousHeight = TestTable.Frame.Height;
			//-- PZDC!!!
			TestTable.LayoutIfNeeded();
			//--
			TestTable.Frame = new CoreGraphics.CGRect(TestTable.Frame.X, TestTable.Frame.Y, TestTable.Frame.Width, TestTable.ContentSize.Height);
			TestTable.Bounds = new CoreGraphics.CGRect(TestTable.Bounds.X, TestTable.Bounds.Y, TestTable.Bounds.Width, TestTable.ContentSize.Height);
			TabH.Constant = TestTable.ContentSize.Height;
			UpdateConstraintsIfNeeded();

			return Tab();
		}

		public nfloat Tab(nfloat? h = null)
		{
			if (h == null)
			{
				var res = (Frame.Height - _previousHeight) + TestTable.Frame.Height;
				return (res < 0) ? -res : res;
			}
			else
			{
				return (Frame.Height - _previousHeight) + h.Value;
			}
		}
    }
}
// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace air
{
    [Register ("TestView")]
    partial class TestView
    {
        [Outlet]
        UIKit.NSLayoutConstraint TabH { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint TestTabHeight { get; set; }


        [Outlet]
        UIKit.UITableView TestTable { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint TestTableHeightConstraint { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint Tth { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (TabH != null) {
                TabH.Dispose ();
                TabH = null;
            }

            if (TestTable != null) {
                TestTable.Dispose ();
                TestTable = null;
            }
        }
    }
}
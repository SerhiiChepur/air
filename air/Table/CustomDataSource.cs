﻿using System;
using Foundation;
using System.Collections.Generic;
using UIKit;

namespace air
{
	public class CustomDataSource : UITableViewDataSource
	{
		public CustomDataSource()
		{
			Data = new List<RowModel>();
		}
		public List<RowModel> Data;
		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = (CustomCell)tableView.DequeueReusableCell("CustomCell");
			if (cell == null)
			{
				cell = CustomCell.CellCreate();
			}
			var item = Data[indexPath.Row];
			cell.Header = item.Title;
			cell.Footer = item.Subtitle;
			cell.SetNeedsUpdateConstraints();
			cell.UpdateConstraints();
			return cell;

		}

		public override nint RowsInSection(UITableView tableView, nint section)
		{
			return (Data != null) ? Data.Count : 0;
		}
	}
}


﻿using System;
using System.Linq;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace air
{
	public class RootDataSource : UITableViewDataSource
	{
		public RootDataSource()
		{
			Data = new List<IRootItem>();
			Data.AddRange(InitData());
		}

		public List<IRootItem> Data { get; set; }


		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell;
			cell = CellFactory(tableView, Data[indexPath.Row]);
			return cell;
		}

		public override nint RowsInSection(UITableView tableView, nint section)
		{
			return(Data != null) ? Data.Count : 0;
		}

		private UITableViewCell CellFactory(UITableView tableView, IRootItem rootItem)
		{
			UITableViewCell cell;
			switch(rootItem.ModelType)
			{
				case EModelType.air:
					{
						var item = (AirModel)rootItem;
						var _cell = (AirCell)tableView.DequeueReusableCell(Constants.CellId_Air);
						if (_cell == null)
						{
							_cell = AirCell.CellCreate();
						}
						_cell.TypeLabel.Text = Enum.GetName(typeof(EModelType), rootItem.ModelType);
						_cell.DescriptionL.Text = string.Format("{0}\n{1}", item.ArrivalDate, item.DepartureDate);
						cell = _cell;
						break;
					}
				case EModelType.autolayoutTrick: 
					{
						var item = (AutolayoutTrickModel)rootItem;
						var _cell = tableView.DequeueReusableCell(Constants.CellId_AutolayoutTrick);
						if (_cell == null)
						{
							_cell = new UITableViewCell(UITableViewCellStyle.Subtitle, Constants.CellId_AutolayoutTrick);
						}
						_cell.TextLabel.Text = item.Title;
						_cell.DetailTextLabel.Text = item.Description;
						_cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
						_cell.BackgroundColor = UIColor.Orange;
						cell = _cell;
						break;
					}
				default: 
					{
						throw new NotImplementedException(string.Format("{0} is not implemented", rootItem.ModelType)); 
					}
			}
			cell.SetNeedsUpdateConstraints();
			cell.UpdateConstraints();
			return cell;
		}

		IEnumerable<IRootItem> InitData()
		{
			return new List<IRootItem>()
			{
				new AirModel()
				{
					FlightValue = "KL3097",
					OperatedValue = "DELTA AIRLINES",
					FlightProgress = "BOARDING IN PROGRESS",
					FlightDelayStatus = "FLIGHT DELAYED",
					DepartureValue = "AMS",
					DepartureFromTo = "Amsterdam - Schipholl",
					DepartureDate = "MON, MAR 24",
					DepartureTimeValue = "08:55 AM",
					DepartureTerminalValue = "D",
					DepartureGateValue = "37",
					DepartureUnknownValue = "08:30 AM",
					SeatsDispSt2 = "25A, 25B",
					FlightDurationValue = "2h 5m",
					ArrivalValue = "JFK",
					ArrivalFromTo = "New York - John F. Kennedy Intl.",
					ArrivalDate = "MON, MAR 24",
					ArrivalTimeValue = "—",
					ArrivalTerminalValue = "D",
					ArrivalGateValue = "37",
					LayoverValue = string.Empty,
					BaggageLabel = "—",
					From = new Geo() { Lattitude = 40.748326427456, Longtittude = -73.9852814732047 },
					To = new Geo() { Lattitude = 49.8439719114576, Longtittude = 24.0286550352285 },
					Subdata = Enumerable.Repeat(new RowModel { Title = "Value", Subtitle = string.Concat(Enumerable.Repeat<string>("adsd\n", new Random().Next(1, 10))) }, new Random().Next(2, 7)).ToList()
				},
				new AutolayoutTrickModel()
				{
					Title = "Autolayout trick",
					Description = "Some trick info",
					Msg = "Ta-daa-a-m!"
				},
				new AirModel()
				{
					FlightValue = "KL3097",
					OperatedValue = "DELTA AIRLINES",
					FlightProgress = "BOARDING IN PROGRESS",
					FlightDelayStatus = "",
					DepartureValue = "AMS",
					DepartureFromTo = "Amsterdam - Schipholl",
					DepartureDate = "MON, MAR 24",
					DepartureTimeValue = "08:55 AM",
					DepartureTerminalValue = "D",
					DepartureGateValue = "37",
					DepartureUnknownValue = "08:30 AM",
					SeatsDispSt2 = "25A, 25B",
					FlightDurationValue = "2h 5m",
					ArrivalValue = "JFK",
					ArrivalFromTo = "New York - John F. Kennedy Intl.",
					ArrivalDate = "MON, MAR 24",
					ArrivalTimeValue = "—",
					ArrivalTerminalValue = "D",
					ArrivalGateValue = "37",
					LayoverValue = "2016 01 01",
					BaggageLabel = "—",
					From = new Geo() { Lattitude = 40.748326427456, Longtittude = -73.9852814732047 },
					To = new Geo() { Lattitude = 49.8439719114576, Longtittude = 24.0286550352285 },
					Subdata = Enumerable.Repeat(new RowModel() { Title = "Value", Subtitle = string.Concat(Enumerable.Repeat<string>("adsd\n", new Random().Next(2, 13))) }, new Random().Next(2, 10)).ToList()
				}

			};
		}
	}
}


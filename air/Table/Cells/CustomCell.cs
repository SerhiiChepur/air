﻿using System;

using Foundation;
using UIKit;

namespace air
{
	public partial class CustomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("CustomCell");
		public static readonly UINib Nib;

		static CustomCell()
		{
			Nib = UINib.FromName("CustomCell", NSBundle.MainBundle);
		}

		protected CustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static CustomCell CellCreate()
		{
			return (CustomCell)Nib.Instantiate(null, null)[0];
		}


		partial void IncButAct(Foundation.NSObject sender)
		{
			this.AutoresizingMask = UIViewAutoresizing.All;
			HeaderVal.Text += HeaderVal.Text;
		}

		public string Header
		{
			get 
			{
				return HeaderVal.Text;
			}
			set
			{
				HeaderVal.Text = value;
			}
		}

		public string Footer
		{
			get
			{
				return SubtitleVal.Text;
			}
			set
			{
				SubtitleVal.Text = value;
			}
		}
	}
}

// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace air
{
    [Register ("CustomCell")]
    partial class CustomCell
    {
        [Outlet]
        UIKit.UILabel HeaderVal { get; set; }


        [Outlet]
        UIKit.UIButton IncButton { get; set; }


        [Outlet]
        UIKit.UILabel SubtitleVal { get; set; }


        [Action ("IncButAct:")]
        partial void IncButAct (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (HeaderVal != null) {
                HeaderVal.Dispose ();
                HeaderVal = null;
            }

            if (IncButton != null) {
                IncButton.Dispose ();
                IncButton = null;
            }

            if (SubtitleVal != null) {
                SubtitleVal.Dispose ();
                SubtitleVal = null;
            }
        }
    }
}
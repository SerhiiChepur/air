﻿using System;

using Foundation;
using UIKit;

namespace air
{
	public partial class AirCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString(Constants.CellId_Air);
		public static readonly UINib Nib;

		static AirCell()
		{
			Nib = UINib.FromName(Constants.CellId_Air, NSBundle.MainBundle);
		}

		protected AirCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public UILabel DescriptionL
		{ 
			get 
			{
				return DescriptionLabel;
			} 
		}

		public UILabel TypeLabel
		{
			get
			{
				return TypeValue;
			}
		}

		public static AirCell CellCreate()
		{
			return (AirCell)Nib.Instantiate(null, null)[0];
		}
	}
}

using Foundation;
using System;
using UIKit;

namespace air
{
    public partial class ModelsTableViewController : UITableViewController
    {
        public ModelsTableViewController (IntPtr handle) : base (handle)
        {
        }

		public RootDataSource DataSrc { get; private set; }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			RootTable.RowHeight = UITableView.AutomaticDimension;
			RootTable.EstimatedRowHeight = 120;
			DataSrc = new RootDataSource();
			RootTable.DataSource = DataSrc;
			RootTable.ReloadData();
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			var item = DataSrc.Data[indexPath.Row];
			UIViewController view = null;
			switch (item.ModelType)
			{
				case EModelType.air:
					{
						view = Storyboard.InstantiateViewController(Constants.VcId_Flight);
						((ViewController)view).Model = (AirModel)DataSrc.Data[indexPath.Row];
						break;
					}

				case EModelType.autolayoutTrick: 
					{ 
						view = Storyboard.InstantiateViewController(Constants.VcId_AutoLayoutTrick);
						((AutoLayoutTrickViewController)view).Model = (AutolayoutTrickModel)DataSrc.Data[indexPath.Row];
						break;
					}
			}

			if (view != null)
			{
				NavigationController.PushViewController(view, true);
			}
			RootTable.DeselectRow(indexPath, true);
		}
    }
}
// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace air
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        UIKit.UILabel ArrivalDate { get; set; }


        [Outlet]
        UIKit.UILabel ArrivalFromTo { get; set; }


        [Outlet]
        UIKit.UILabel ArrivalGateValue { get; set; }


        [Outlet]
        UIKit.UILabel ArrivalTerminalValue { get; set; }


        [Outlet]
        UIKit.UILabel ArrivalTimeValue { get; set; }


        [Outlet]
        UIKit.UILabel ArrivalValue { get; set; }


        [Outlet]
        UIKit.UILabel BaggageLabel { get; set; }


        [Outlet]
        UIKit.UILabel DepartureDate { get; set; }


        [Outlet]
        UIKit.UILabel DepartureFromTo { get; set; }


        [Outlet]
        UIKit.UILabel DepartureGateValue { get; set; }


        [Outlet]
        UIKit.UILabel DepartureTerminalValue { get; set; }


        [Outlet]
        UIKit.UILabel DepartureTimeValue { get; set; }


        [Outlet]
        UIKit.UILabel DepartureUnknownValue { get; set; }


        [Outlet]
        UIKit.UILabel DepartureValue { get; set; }


        [Outlet]
        UIKit.UILabel FlightDelayStatus { get; set; }


        [Outlet]
        UIKit.UILabel FlightDurationValue { get; set; }


        [Outlet]
        UIKit.UILabel FlightProgress { get; set; }


        [Outlet]
        UIKit.UILabel FlightValue { get; set; }


        [Outlet]
        UIKit.UIButton InBut { get; set; }


        [Outlet]
        UIKit.UILabel LayoverBaggageLabel { get; set; }


        [Outlet]
        UIKit.UILabel LayoverValue { get; set; }


        [Outlet]
        UIKit.UILabel OperatedValue { get; set; }


        [Outlet]
        UIKit.UIView ProgView { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint ProgViewHConstraint { get; set; }


        [Outlet]
        UIKit.UILabel SeatsDisp { get; set; }


        [Outlet]
        UIKit.UILabel SeatsDispSt2 { get; set; }


        [Outlet]
        UIKit.UIButton Subview { get; set; }


        [Action ("Inc:")]
        partial void Inc (UIKit.UIButton sender);


        [Action ("Incc:")]
        partial void Incc (UIKit.UIButton sender);


        [Action ("SubviewClicked:")]
        partial void SubviewClicked (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (ArrivalDate != null) {
                ArrivalDate.Dispose ();
                ArrivalDate = null;
            }

            if (ArrivalFromTo != null) {
                ArrivalFromTo.Dispose ();
                ArrivalFromTo = null;
            }

            if (ArrivalGateValue != null) {
                ArrivalGateValue.Dispose ();
                ArrivalGateValue = null;
            }

            if (ArrivalTerminalValue != null) {
                ArrivalTerminalValue.Dispose ();
                ArrivalTerminalValue = null;
            }

            if (ArrivalTimeValue != null) {
                ArrivalTimeValue.Dispose ();
                ArrivalTimeValue = null;
            }

            if (ArrivalValue != null) {
                ArrivalValue.Dispose ();
                ArrivalValue = null;
            }

            if (BaggageLabel != null) {
                BaggageLabel.Dispose ();
                BaggageLabel = null;
            }

            if (DepartureDate != null) {
                DepartureDate.Dispose ();
                DepartureDate = null;
            }

            if (DepartureFromTo != null) {
                DepartureFromTo.Dispose ();
                DepartureFromTo = null;
            }

            if (DepartureGateValue != null) {
                DepartureGateValue.Dispose ();
                DepartureGateValue = null;
            }

            if (DepartureTerminalValue != null) {
                DepartureTerminalValue.Dispose ();
                DepartureTerminalValue = null;
            }

            if (DepartureTimeValue != null) {
                DepartureTimeValue.Dispose ();
                DepartureTimeValue = null;
            }

            if (DepartureUnknownValue != null) {
                DepartureUnknownValue.Dispose ();
                DepartureUnknownValue = null;
            }

            if (DepartureValue != null) {
                DepartureValue.Dispose ();
                DepartureValue = null;
            }

            if (FlightDelayStatus != null) {
                FlightDelayStatus.Dispose ();
                FlightDelayStatus = null;
            }

            if (FlightDurationValue != null) {
                FlightDurationValue.Dispose ();
                FlightDurationValue = null;
            }

            if (FlightProgress != null) {
                FlightProgress.Dispose ();
                FlightProgress = null;
            }

            if (FlightValue != null) {
                FlightValue.Dispose ();
                FlightValue = null;
            }

            if (LayoverBaggageLabel != null) {
                LayoverBaggageLabel.Dispose ();
                LayoverBaggageLabel = null;
            }

            if (LayoverValue != null) {
                LayoverValue.Dispose ();
                LayoverValue = null;
            }

            if (OperatedValue != null) {
                OperatedValue.Dispose ();
                OperatedValue = null;
            }

            if (ProgView != null) {
                ProgView.Dispose ();
                ProgView = null;
            }

            if (ProgViewHConstraint != null) {
                ProgViewHConstraint.Dispose ();
                ProgViewHConstraint = null;
            }

            if (SeatsDispSt2 != null) {
                SeatsDispSt2.Dispose ();
                SeatsDispSt2 = null;
            }

            if (Subview != null) {
                Subview.Dispose ();
                Subview = null;
            }
        }
    }
}
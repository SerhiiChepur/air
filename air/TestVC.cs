using Foundation;
using System;
using UIKit;
using System.Linq;

namespace air
{
    public partial class TestVC : UIViewController
    {
		public CustomDataSource DataSrc { get; set; }

        public TestVC (IntPtr handle) : base (handle)
        {
        }

		partial void SomeCl(UIButton sender)
		{
			DataSrc = new CustomDataSource();
			DataSrc.Data.AddRange(new RowModel[]
			{
				new RowModel(){ Title = "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest", Subtitle = "Subtitle test"},
				new RowModel(){ Title = "Test2", Subtitle = "Subtitle test"},
			}
			);
			var fr = TestContainer.Frame;
			var n = TestView.Create();
			for (var c = 0; c < n.Subviews.Count(); c++)
			{
				var tview = n.Subviews[c] as UITableView;
				if (tview != null)
				{
					((UITableView)n.Subviews[c]).DataSource = DataSrc;
					((UITableView)n.Subviews[c]).RowHeight = UITableView.AutomaticDimension;
					((UITableView)n.Subviews[c]).EstimatedRowHeight = 220;
					break;
				}
			}
			TestContainer.Frame = new CoreGraphics.CGRect(TestContainer.Frame.X, TestContainer.Frame.Y, n.Frame.Width, n.Frame.Height);
			//n.Frame = fr;
			TestContainer.Add(n);
			View.LayoutSubviews();
		}
	}
}
﻿using System;
namespace air
{
	public class AutolayoutTrickModel : IRootItem
	{
		public string Description { get; internal set; }

		public EModelType ModelType
		{
			get
			{
				return EModelType.autolayoutTrick;
			}
		}

		public string Title { get; set; }
		public string Msg { get; set; }
	}
}


﻿using System;
using System.Collections.Generic;

namespace air
{
	public class AirModel : IRootItem
	{
		/// <summary>
		/// The airplane ID
		/// </summary>
		/// <value>The flight.</value>
		public string FlightValue { get; set; }
		public string OperatedValue { get; set; }
		public string FlightProgress { get; set; }
		public string FlightDelayStatus { get; set; }
		public string DepartureValue { get; set; }
		public string DepartureFromTo { get; set; }
		public string DepartureDate { get; set; }
		public string DepartureTimeValue { get; set; }
		public string DepartureTerminalValue { get; set; }
		public string DepartureGateValue { get; set; }
		public string DepartureUnknownValue { get; set; }
		public string SeatsDispSt2 { get; set; }
		public string FlightDurationValue { get; set; }
		public string ArrivalValue { get; set; }
		public string ArrivalFromTo { get; set; }
		public string ArrivalDate { get; set; }
		public string ArrivalTimeValue { get; set; }
		public string ArrivalTerminalValue { get; set; }
		public string ArrivalGateValue { get; set; }
		public string LayoverValue { get; set; }
		public string BaggageLabel { get; set; }
		//--
		public Geo From { get; set; }
		public Geo To { get; set; }

		public EModelType ModelType
		{
			get
			{
				return EModelType.air;
			}
		}

		public List<RowModel> Subdata { get; set;}
	}

	public class Geo
	{
		public double Lattitude { get; set; }
		public double Longtittude { get; set; }
	}
}


﻿namespace air
{
	public static class Constants
	{
		public const string CellId_Air = "AirCell";

		public const string CellId_AutolayoutTrick = "AutoLTrickCell";

		public const string VcId_RootNav ="NavVCtrl";

		public const string VcId_RootTab = "RootTabCtrl";

		public const string VcId_Flight = "FlightVc";

		public static string VcId_AutoLayoutTrick = "AutoLayoutTrick";
	}
}


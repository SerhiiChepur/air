// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace air
{
    [Register ("AutoLayoutTrickViewController")]
    partial class AutoLayoutTrickViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton LandButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LeftView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView RightView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TextFromModel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TopView { get; set; }

        [Action ("LandClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void LandClicked (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (LandButton != null) {
                LandButton.Dispose ();
                LandButton = null;
            }

            if (LeftView != null) {
                LeftView.Dispose ();
                LeftView = null;
            }

            if (RightView != null) {
                RightView.Dispose ();
                RightView = null;
            }

            if (TextFromModel != null) {
                TextFromModel.Dispose ();
                TextFromModel = null;
            }

            if (TopView != null) {
                TopView.Dispose ();
                TopView = null;
            }
        }
    }
}
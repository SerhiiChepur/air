using Foundation;
using System;
using UIKit;
using System.ComponentModel;

namespace air
{
    public partial class FlightView : UIView
    {
        public FlightView (IntPtr handle) : base (handle)
        {
        }

		[Export("IsHopOff"), Browsable(true)]
		public bool IsHopOff { get; set; }

		public override void MovedToSuperview()
		{
			base.MovedToSuperview();
		}
    }
}
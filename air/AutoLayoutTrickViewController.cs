using Foundation;
using System;
using UIKit;

namespace air
{
    public partial class AutoLayoutTrickViewController : UIViewController
    {
        public AutoLayoutTrickViewController (IntPtr handle) : base (handle)
        {
        }

		public AutolayoutTrickModel Model { get; set; } 

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			if (Model != null)
			{
				TextFromModel.Text = Model.Title;
			}
		}

		partial void LandClicked(UIButton sender)
		{
			UIAlertView alert = new UIAlertView()
			{
				Title = "Test tricks app title",
				Message = Model.Msg
			};
			alert.AddButton("OK");
			alert.Show();
		}
	}
}
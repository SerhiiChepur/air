// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace air
{
    [Register ("TestVC")]
    partial class TestVC
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TestContainer { get; set; }

        [Action ("SomeCl:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SomeCl (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (TestContainer != null) {
                TestContainer.Dispose ();
                TestContainer = null;
            }
        }
    }
}
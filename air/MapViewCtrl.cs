using Foundation;
using System;
using UIKit;
using CoreLocation;
using MapKit;

namespace air
{
    public partial class MapViewCtrl : UIViewController
    {
        public MapViewCtrl (IntPtr handle) : base (handle)
        {
        }

		public Geo ForShowing { get; set; }

		public override void ViewDidAppear(bool animated)
		{
			if (ForShowing != null)
			{
				CLLocationCoordinate2D mapCenter = new CLLocationCoordinate2D(ForShowing.Lattitude, ForShowing.Longtittude);
				MKCoordinateRegion mapRegion = MKCoordinateRegion.FromDistance(mapCenter, 100, 100);
				Map.ShowsBuildings = true;
				Map.SetRegion(mapRegion, true);
				//--
				Map.AddAnnotations(new MKPointAnnotation() { Coordinate = mapCenter, Title = "Here" });
			}
		}
    }
}
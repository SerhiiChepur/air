﻿using System;
using UIKit;
using System.Linq;

namespace air
{
	public partial class ViewController : UIViewController
	{
		public AirModel Model { get; set; }
		public AirModel ModelTwo { get; set; }
		public bool Two { get; set; }
		public CustomDataSource DataSrc { get; set; }
		public int Idd { get; set; }

		private TestView _loadedView;

		partial void SubviewClicked(UIButton sender)
		{
			FillViewContrainer(Model);
		}

		void FillViewContrainer(AirModel model)
		{
			if (_loadedView == null)
			{
				DataSrc = new CustomDataSource();
				DataSrc.Data.AddRange(model.Subdata);
				_loadedView = TestView.Create();
				_loadedView.TTable.DataSource = DataSrc;
				_loadedView.TTable.RowHeight = UITableView.AutomaticDimension;
				_loadedView.TTable.EstimatedRowHeight = 120;
				var nHeight = _loadedView.RecalcHeight();
				ProgView.Frame = new CoreGraphics.CGRect(ProgView.Frame.X, ProgView.Frame.Y, _loadedView.Frame.Width, nHeight);
				_loadedView.Frame = new CoreGraphics.CGRect(0, 0, _loadedView.Frame.Width, nHeight);
				_loadedView.Bounds = new CoreGraphics.CGRect(0, 0, _loadedView.Bounds.Width, nHeight);
				_loadedView.UpdateConstraintsIfNeeded();
				ProgView.Add(_loadedView);
				View.LayoutSubviews();
				ProgViewHConstraint.Constant = _loadedView.Frame.Height;
			}
			else
			{
				Idd++;
				DataSrc.Data.Add(new RowModel() { Title = string.Format("N {0}", Idd), Subtitle = "test" });
				_loadedView.TTable.ReloadData();
				var nHeight = _loadedView.RecalcHeight();
				ProgView.Frame = new CoreGraphics.CGRect(ProgView.Frame.X, ProgView.Frame.Y, _loadedView.Frame.Width, nHeight);
				ProgViewHConstraint.Constant = nHeight;

				_loadedView.Frame = new CoreGraphics.CGRect(0, 0, _loadedView.Frame.Width, nHeight);
				_loadedView.Bounds = new CoreGraphics.CGRect(0, 0, _loadedView.Bounds.Width, nHeight);
				_loadedView.UpdateConstraintsIfNeeded();

			}
		}

		protected ViewController(IntPtr handle) : base(handle)
		{
			Idd = 0;
			// Note: this .ctor should not contain any initialization logic.
			Model = new AirModel()
			{
				FlightValue 				= "KL3097",
				OperatedValue 				= "DELTA AIRLINES",
				FlightProgress				 = "BOARDING IN PROGRESS",
				FlightDelayStatus 			= "FLIGHT DELAYED",
				DepartureValue 				= "AMS",
				DepartureFromTo 			= "Amsterdam - Schipholl",
				DepartureDate 				= "MON, MAR 24",
				DepartureTimeValue				 = "08:55 AM",
				DepartureTerminalValue 		= "D",
				DepartureGateValue 			= "37",
				DepartureUnknownValue 		= "08:30 AM",
				SeatsDispSt2 				= "25A, 25B",
				FlightDurationValue 		= "2h 5m",
				ArrivalValue 				= "JFK",
				ArrivalFromTo 				= "New York - John F. Kennedy Intl.",
				ArrivalDate					 = "MON, MAR 24",
				ArrivalTimeValue			 = "—",
				ArrivalTerminalValue		 = "D",
				ArrivalGateValue			 = "37",
				LayoverValue 				= string.Empty,
				BaggageLabel 				= "—",
				From = new Geo() { Lattitude = 40.748326427456, Longtittude = -73.9852814732047 },
				To = new Geo() { Lattitude = 49.8439719114576, Longtittude = 24.0286550352285 }
			};
			ModelTwo = new AirModel()
			{
				FlightValue = "KL3097",
				OperatedValue = "DELTA AIRLINES",
				FlightProgress = "BOARDING IN PROGRESS",
				FlightDelayStatus = "",
				DepartureValue = "AMS",
				DepartureFromTo = "Amsterdam - Schipholl",
				DepartureDate = "MON, MAR 24",
				DepartureTimeValue = "08:55 AM",
				DepartureTerminalValue = "D",
				DepartureGateValue = "37",
				DepartureUnknownValue = "08:30 AM",
				SeatsDispSt2 = "25A, 25B",
				FlightDurationValue = "2h 5m",
				ArrivalValue = "JFK",
				ArrivalFromTo = "New York - John F. Kennedy Intl.",
				ArrivalDate = "MON, MAR 24",
				ArrivalTimeValue = "—",
				ArrivalTerminalValue = "D",
				ArrivalGateValue = "37",
				LayoverValue = "2016 01 01",
				BaggageLabel = "—",
				From = new Geo() { Lattitude = 40.748326427456, Longtittude = -73.9852814732047 },
				To = new Geo() { Lattitude = 49.8439719114576, Longtittude = 24.0286550352285 }
			};
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Fill(Model);
		}

		public void Fill(AirModel Model)
		{
			FlightValue.Text = Model.FlightValue;
			FlightValue.Text = Model.FlightValue;
			OperatedValue.Text = Model.OperatedValue;
			FlightProgress.Text = Model.FlightProgress;
			FlightDelayStatus.Text = Model.FlightDelayStatus;
			DepartureValue.Text = Model.DepartureValue;
			DepartureFromTo.Text = Model.DepartureFromTo;
			DepartureDate.Text = Model.DepartureDate;
			DepartureTimeValue.Text = Model.DepartureTimeValue;
			DepartureTerminalValue.Text = Model.DepartureTerminalValue;
			DepartureGateValue.Text = Model.DepartureGateValue;
			DepartureUnknownValue.Text = Model.DepartureUnknownValue;
			SeatsDispSt2.Text = Model.SeatsDispSt2;
			FlightDurationValue.Text = Model.FlightDurationValue;
			ArrivalValue.Text = Model.ArrivalValue;
			ArrivalFromTo.Text = Model.ArrivalFromTo;
			ArrivalDate.Text = Model.ArrivalDate;
			ArrivalTimeValue.Text = Model.ArrivalTimeValue;
			ArrivalTerminalValue.Text = Model.ArrivalTerminalValue;
			ArrivalGateValue.Text = Model.ArrivalGateValue;
			if (!string.IsNullOrWhiteSpace(Model.LayoverValue))
			{
				LayoverBaggageLabel.Text = "LAYOVER:";
				LayoverValue.Text = Model.LayoverValue;
				BaggageLabel.Text = string.Empty;
			}
			else
			{
				LayoverBaggageLabel.Text = "BAGGAGE";
				BaggageLabel.Text = Model.BaggageLabel;
				LayoverValue.Text = string.Empty;
			}
			if (Model.Subdata != null)
			{
				if (Model.Subdata.Any())
				{
					FillViewContrainer(Model);
				}
			}
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, Foundation.NSObject sender)
		{
			Geo tmp = null;
			if (segue.Identifier == "departureSegue")
			{
				tmp = Model.From;
			}
			if (segue.Identifier == "arrivalSegue")
			{ 
				tmp = Model.To;
			}
			var destination = segue.DestinationViewController as MapViewCtrl;
			if (destination != null & tmp != null)
			{
				destination.ForShowing = tmp;
			}
		}

		partial void Inc(UIButton sender)
		{
			SeatsDispSt2.Text = (string.IsNullOrWhiteSpace(SeatsDispSt2.Text))? "ssdfsdfsdfsd" : SeatsDispSt2.Text + " " + SeatsDispSt2.Text;
			if (Two)
			{
				Fill(Model);
				Two = false;
			}
			else 
			{
				Fill(ModelTwo);
				Two = true;
			}
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

